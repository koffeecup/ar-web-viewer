const isIpad = () => {
  var ua = window.navigator.userAgent;
  if (ua.indexOf("iPad") > -1) {
    return true;
  }

  if (ua.indexOf("Macintosh") > -1) {
    try {
      document.createEvent("TouchEvent");
      return true;
    } catch (e) {}
  }

  return false;
};

const isMobileDevice = () => {
  return (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    ) || isIpad()
  );
};

const clearWindowSelection = () => {
  if(document.selection && document.selection.empty) {
      document.selection.empty();
  } else if(window.getSelection) {
      const sel = window.getSelection();
      sel.removeAllRanges();
  }
}

function addModelListeners(modelSelector) {
  const modelViewerEl = document.querySelector(modelSelector);
  const errorEl = document.querySelector(`${modelSelector} .ar-error`);
  const errorBtnCloseEl = document.querySelector(
    `${modelSelector} .ar-error__close-btn`
  );
  let originalCameraOrbit, originalFieldOfView;

  const progressEl = document.querySelector(`${modelSelector} .ar-progress`);
  const barEl = document.querySelector(`${modelSelector} .ar-progress__bar`);

  const arDesktopBtn = document.querySelector(
    `${modelSelector} .ar-desktop-btn`
  );
  const arDesktopInfo = document.querySelector(
    `${modelSelector} .ar-desktop-info`
  );
  const arDesktopInfoCloseBtn = document.querySelector(
    `${modelSelector} .ar-desktop-info__close-btn`
  );

  if (!modelViewerEl) {
    return;
  }

  modelViewerEl.addEventListener(
    "progress",
    (event) => {
      const totalProgress = event.detail.totalProgress;
      progressEl.classList.toggle("show", totalProgress < 1);
      barEl.style.transform = `scaleX(${totalProgress})`;
    },
    { passive: true }
  );

  modelViewerEl.addEventListener(
    "ar-status",
    (event) => {
      event.preventDefault();
      event.stopPropagation();

      if (event.detail.status === "failed") {
        errorEl.classList.add("show");
      }
    },
    { passive: true }
  );

  modelViewerEl.addEventListener(
    "load",
    (_event) => {
      const isMobile = isMobileDevice();

      if (isMobile) {
        return;
      }

      arDesktopBtn.classList.add("show");
      arDesktopBtn.addEventListener(
        "click",
        (_event) => {
          arDesktopInfo.classList.add("show");
        },
        { passive: true }
      );

      arDesktopInfoCloseBtn.addEventListener(
        "click",
        (_event) => {
          arDesktopInfo.classList.remove("show");
        },
        { passive: true }
      );

      originalCameraOrbit = modelViewerEl.getCameraOrbit();
      originalFieldOfView = modelViewerEl.getFieldOfView();
    },
    { passive: true }
  );

  modelViewerEl.addEventListener(
    "dblclick",
    function (event) {
      event.preventDefault();
      event.stopPropagation();
      clearWindowSelection();

      modelViewerEl.cameraOrbit = originalCameraOrbit;
      modelViewerEl.fieldOfView = originalFieldOfView;
    }
  );

  errorBtnCloseEl.addEventListener(
    "click",
    () => {
      errorEl.classList.remove("show");
    },
    { passive: true }
  );
}

// Partial HACK for browser history.back() issue;
// https://github.com/google/model-viewer/issues/1513
if (!window.location.href.includes("no-ar-fallback")) {
  history.pushState({}, "empty", window.location.href);
}
// HACK END

addModelListeners(".hiphop-model");
addModelListeners(".jumping-model");
addModelListeners(".barry-model");
