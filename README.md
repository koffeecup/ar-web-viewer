# Displaying model in the browser and AR app.

For AR your device needs to support AR Kit (Apple QuickLook app) or AR Core (Android SceneViewer). Available in the most modern devices by default. 

In order to display model in the browser we will use `model-viewer` web component.

Model viewer documentation:
https://modelviewer.dev/

How to integrate model-viewer with your site?

1. Make sure you have valid glb or gltf model file.
2. If you want to support iOS devices to correctly display models in QuickLook AR App - make sure you also have valid usdz file.
3. Put files on some hosting / public server (eg. AWS s3 bucket).
4. Set proper Content-Type header for model files accordingly:
- glb header `Content-Type: model/gltf-binary`,
- gltf header `Content-Type: model/gltf+json`,
- usdz header `Content-Type: model/vnd.pixar.usd`.
5. Provide all necessary scripts and polyfills to support as most browsers as possible.

```
<script src="https://unpkg.com/@webcomponents/webcomponentsjs@2.1.3/webcomponents-loader.js"></script>
<script src="https://unpkg.com/intersection-observer@0.5.1/intersection-observer.js"></script>
<script src="https://unpkg.com/resize-observer-polyfill@1.5.0/dist/ResizeObserver.js"></script>
<script src="https://unpkg.com/fullscreen-polyfill@1.0.2/dist/fullscreen.polyfill.js"></script>
<script src="https://unpkg.com/focus-visible@5.0.1/dist/focus-visible.js" defer></script>


<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>
<script nomodule src="https://unpkg.com/@google/model-viewer/dist/model-viewer-legacy.js"></script>
```

6. Add <model-viewer> web component markup to your HTML page.

```
  <model-viewer 
    class="model-viewer hiphop-model"
    src="Copy link here to your *.glb|*.gltf file" 
    ar
    ar-modes="scene-viewer quick-look" 
    ar-scale="auto" 
    autoplay
    ios-src="Copy link here to your *.usdz file"
    alt="Provide alternative text for your model here" 
    quick-look-browsers="safari chrome" 
    camera-controls
  >
  </model-viewer>
```

7. Test your model before you push it live! :)

## Features

- Write SCSS and modern JavaScript code in `src` and build minified, transpiled code for production in `dist`
- Live reloading with webpack-dev-server
- ES6+ to ES5 transpilation, bundling, and minification
- SCSS to CSS transpilation, bundling, autoprefixing, and minification
- Automatic copying of HTML and static assets from `src` to `dist` folders

## Usage

- Available commands:
  - `npm run build`: Build files to the `dist` folder. Transpiles down to ES5 and bundles all JS into `app.bundle.js`. Transpiles SCSS to CSS and adds prefixing into `style.bundle.css`. Copies static assets and HTML over, and bundled CSS and JS gets added to HTML file.
  - `npm run start:dev`: Run `webpack-dev-server` at `localhost:9000`. Includes live reloading on any Javascript/SCSS/HTML changes.
  - `npm run start`: Builds files and runs a local production server on `localhost:8080` with `http-server`.
  
